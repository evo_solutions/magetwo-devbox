### ENVIRONMENT VARIABLES
export DOCKER_PROJECT_NAME="magetwo"
export DOCKER_PROJECT_ROOT="/Users/leandro/Projects/evosolutions/magetwo/source/"
export DOCKER_PROJECT_MAIN_DNS="magetwo.local"

### CONTAINER UTILITIES
alias mage-bin="mage-cli-exec php /var/www/html/bin/magento";
alias mage-cli-exec="mage-compose run --rm php-cli";
alias mage-cli-root="mage-cli-exec -u0 zsh";
alias mage-cli="mage-cli-exec zsh";
alias mage-compile="mage-cli-exec compile";
alias mage-compose="docker-compose -f ${DOCKER_PROJECT_ROOT}../docker-compose.yml"
alias mage-cron="mage-compose up -d cron";
alias mage-db-exec="docker exec ${DOCKER_PROJECT_NAME}_db mysql -umagento -pmagento magento";
alias mage-db="mage-compose up -d db && docker exec -it ${DOCKER_PROJECT_NAME}_db bash";
alias mage-deploy="mage-cli-exec deploy";
alias mage-down="mage-compose down -v --remove-orphans";
alias mage-flush-dirs="rm -rfv ${DOCKER_PROJECT_ROOT}/generated/* ${DOCKER_PROJECT_ROOT}/pub/static/* ${DOCKER_PROJECT_ROOT}/var/*"
alias mage-fpm="docker exec -it ${DOCKER_PROJECT_NAME}_php-fpm bash";
alias mage-go="cd ${DOCKER_PROJECT_ROOT}";
alias mage-http="docker exec -it ${DOCKER_PROJECT_NAME}_http sh";
alias mage-install="mage-cli-exec install";
alias mage-logs="mage-compose logs -f";
alias mage-n98="mage-cli-exec n98";
alias mage-run="${DOCKER_PROJECT_ROOT}../tools/run";
alias mage-start="mage-compose start tls";
alias mage-stop="mage-compose stop";
alias mage-up-logs="mage-up && mage-logs";
alias mage-up="mage-compose up -d tls";
alias mage-upgrade="mage-cli-exec upgrade";
alias mage-which="echo Currently running \'$DOCKER_PROJECT_NAME\' from \'$DOCKER_PROJECT_ROOT\'";
