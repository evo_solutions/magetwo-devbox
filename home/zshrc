###
# HOST UTILITIES
###
alias hg="history | grep"
alias ll="ls -alhs"
alias ls='ls -GFhs'
alias mage-flushdirs="rm -rfv ${MAGENTO_ROOT}/generated/* ${MAGENTO_ROOT}/pub/static/* ${MAGENTO_ROOT}/var/*"
alias mage="php ${MAGENTO_ROOT}/bin/magento"
alias magento="php ${MAGENTO_ROOT}/bin/magento"
alias mod="stat -c \"%U.%G %a %n [%F]\"" # => Unix
alias opened-ports="sudo lsof -i -n -P | grep TCP"
alias please=sudo

###
# GIT ALIASES
###
alias g="git"
alias ga="git add"
alias gb="git branch"
alias gbl="git show-branch --list"
alias gc="git commit"
alias gca="git commit --amend"
alias gcam="git commit --amend --no-edit"
alias gk="gitk"
alias gl="git log"
alias gg="git pull"
alias ggupstream="git pull upstream"
alias gr="git reset"
alias grb="git rebase"
alias gm="git merge"
alias go="git checkout"
alias gp="git push"
alias gpa="git push --all"
alias gpf="git push --force"
alias gpaf="git push --all --force"
alias gss="git status -v -b --show-stash --ahead-behind"
alias gs="git status"
alias gt="git stash -u"
alias gta="git stash apply"
alias gtd="git stash drop"
alias gtp="git stash pop"
alias gu="git up"

alias gum="echo \"\n\$(tput setab 4)Pulling latest changes from Upstream's remote:\$(tput sgr 0) \" \
&& gu \
&& echo \"\n\$(tput setab 4)Pushing to origin:\$(tput sgr 0) \" \
&& gp -f origin master \
&& echo \"\n\$(tput setab 4)Installing updated dependencies:\$(tput sgr 0) \" \
&& composer install -vvv"

alias gum2="echo \"\n\$(tput setab 4)Changing to master:\$(tput sgr 0) \" \
&& go master \
&& echo \"\n\$(tput setab 4)Pulling latest changes from Upstream's remote:\$(tput sgr 0) \" \
&& ggupstream master \
&& echo \"\n\$(tput setab 4)Pushing to origin:\$(tput sgr 0) \" \
&& gp -f \
&& echo \"\n\$(tput setab 4)Installing updated dependencies:\$(tput sgr 0) \" \
&& composer install -vvv"

alias gsync="ORIGINAL_BRANCH=\$(git rev-parse --abbrev-ref HEAD) \
&& echo \"\n\$(tput setab 2)>>>> SYNCHRONIZING '\$ORIGINAL_BRANCH' WITH UPSTREAM'S MASTER\$(tput sgr 0)\n\" \
&& gum \
&& echo \"\n\$(tput setab 4)Current state:\$(tput sgr 0) \" \
&& gs \
&& echo \"\n\$(tput setab 4)Stashing current working tree:\$(tput sgr 0) \" \
&& gt -u -m \"backup for master rebase\" \
&& echo \"\n\$(tput setab 1)Rebasing \$ORIGINAL_BRANCH with master:\$(tput sgr 0)\n\" \
&& grb master -Xours \
&& echo \"\n\$(tput setab 3)Popping stash:\$(tput sgr 0) \" \
&& gtp \
&& echo \"\n\$(tput setaf 2)ALL DONE! =]\$(tput sgr 0) \""

alias gsync2="ORIGINAL_BRANCH=\$(git rev-parse --abbrev-ref HEAD) \
&& echo \"\n\$(tput setab 2)>>>> SYNCHRONIZING '\$ORIGINAL_BRANCH' WITH UPSTREAM'S MASTER\$(tput sgr 0)\n\" \
&& gum \
&& echo \"\n\$(tput setab 4)Current state:\$(tput sgr 0) \" \
&& gs \
&& echo \"\n\$(tput setab 4)Stashing current working tree:\$(tput sgr 0) \" \
&& gt -u -m \"backup for master rebase\" \
&& echo \"\n\$(tput setab 4)Returning to original branch (\$ORIGINAL_BRANCH):\$(tput sgr 0) \" \
&& go \$ORIGINAL_BRANCH \
&& echo \"\n\$(tput setab 1)Rebasing with master:\$(tput sgr 0)\n\" \
&& grb master -Xours \
&& echo \"\n\$(tput setab 3)Popping stash:\$(tput sgr 0) \" \
&& gtp \
&& echo \"\n\$(tput setaf 2)ALL DONE! =]\$(tput sgr 0) \""

###
# THEME OPTIONS
###
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(composer git ruby sudo node)

#source $ZSH/oh-my-zsh.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
export PATH="/usr/local/opt/ncurses/bin:$PATH"

##################
# Theme options
##################

POWERLEVEL9K_CONTEXT_TEMPLATE='%n'
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND='white'
POWERLEVEL9K_BATTERY_CHARGING='yellow'
POWERLEVEL9K_BATTERY_CHARGED='green'
POWERLEVEL9K_BATTERY_DISCONNECTED='$DEFAULT_COLOR'
POWERLEVEL9K_BATTERY_LOW_THRESHOLD='10'
POWERLEVEL9K_BATTERY_LOW_COLOR='red'
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=''
POWERLEVEL9K_BATTERY_ICON='\uf1e6 '
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%F{014}\u2570%F{cyan}\uF054%F{073}\uF054%F{109}\uF054%f "
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_UNTRACKED_ICON='?'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon context root_indicator dir_writable dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time time php_version node_version ram load background_jobs)

#POWERLEVEL9K_SHORTEN_STRATEGY="truncate_middle"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1

POWERLEVEL9K_NODE_VERSION_BACKGROUND='blue'

POWERLEVEL9K_TIME_ICON=""
POWERLEVEL9K_TIME_FORMAT="%D{\uf017 %H:%M \uf073 %d/%m}"
POWERLEVEL9K_TIME_BACKGROUND='white'
POWERLEVEL9K_RAM_BACKGROUND='yellow'
POWERLEVEL9K_LOAD_CRITICAL_BACKGROUND="white"
POWERLEVEL9K_LOAD_WARNING_BACKGROUND="white"
POWERLEVEL9K_LOAD_NORMAL_BACKGROUND="white"
POWERLEVEL9K_LOAD_CRITICAL_FOREGROUND="red"
POWERLEVEL9K_LOAD_WARNING_FOREGROUND="yellow"
POWERLEVEL9K_LOAD_NORMAL_FOREGROUND="black"
POWERLEVEL9K_LOAD_CRITICAL_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_LOAD_WARNING_VISUAL_IDENTIFIER_COLOR="yellow"
POWERLEVEL9K_LOAD_NORMAL_VISUAL_IDENTIFIER_COLOR="green"
POWERLEVEL9K_HOME_ICON=''
POWERLEVEL9K_HOME_SUB_ICON=''
POWERLEVEL9K_FOLDER_ICON=''
POWERLEVEL9K_STATUS_VERBOSE=true
POWERLEVEL9K_STATUS_CROSS=true
