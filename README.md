Docker Environment Magento 2.3
==============================

Containers for Magento 2 project development environment.

### How To Use

To make your setup process more efficient open up a second tab in your Shell client and download all needed docker images with the following command. It will happen automatically during the next steps, but since it might take a while depending on your connection it would be wise to run it separately

        $ docker pull library/nginx:alpine \
        && docker pull mailhog/mailhog \
        && docker pull medeirosleandro/elasticsearch:6.5 \
        && docker pull medeirosleandro/php:7.2-cli \
        && docker pull medeirosleandro/php:7.2-fpm \
        && docker pull medeirosleandro/tls:latest \
        && docker pull medeirosleandro/varnish:latest \
        && docker pull percona:5.7.19 \
        && docker pull redis:alpine

1. Clone this repository

        $ git clone git@bitbucket.org:evo_solutions/magetwo-devbox.git magetwo

2. Set the devbox aliases's exported variables accordingly and then source them into your shell client startup configuration

        $ nano ./mage_aliases.sh
        $ echo "\n\n###\n# MAGE TWO DEVBOX\n###\nsource $(pwd)/mage_aliases.sh\n" | tee -a ~/.zshrc
        $ source ~/.zshrc

    If you want to use multiple instances of this devbox with different projects make sure to check out my [Project Switcher](https://bitbucket.org/evo_solutions/project-switcher)

4. Copy your SSH keys, which must be configured on your GitHub & BitBucket accounts, into the _home/ssh/_. If still don't have any keys in your computer please follow [this tutorial](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) before proceding.

        $ cp ~/.ssh/* ./home/ssh/
        $ chmod 755 ./home/ssh/*

5. Edit *auth.json* to copy your composer keys to the PHP-Cli container (optional):

        $ nano ./home/composer/auth.json

6. Copy the custom local hosts of the project to your hosts file

        $ cat ./home/localhosts | sudo tee -a /etc/hosts

    In case you wish to use custom local DNS be sure to also change them in the Environment settings file under _./home/magento/env.php_

7. \[MAC USERS ONLY] Setup your NFS to be used with docker:

    Catalina or newer:

        $ tools/setup_native_nfs_docker_osx.sh

    Older OSX versions:

        $ tools/setup_native_nfs_docker_osx_legacy.sh

8. Clone your code base under _<PROJECT_ROOT>/source_ dir

        $ git clone git@bitbucket.org:evo_solutions/magetwo-platform.git source

9. SKIP THIS IF YOU WANT A CLEAN INSTALATION!!!
   Import your database by copying it under _<PROJECT_ROOT>/home/dump/_ and running the following

        $ # log into the DB container
        $ mage-db
        $ # import your file using the utility
        $ db < <your_dump_file.sql>

        $ # optionally run it with root privileges
        $ dbroot < <your_dump_file.sql>

10. Install the dependencies

        $ mage-cli-exec composer install -vvv

11. SKIP THIS IF YOU ALREADY IMPORTED YOUR DB AND HAVE THE ENV.PHP FILE

        $ mage-install

12. Run a Magento full deploy (database upgrade, config import, classes generation, static content deployment)

        $ mage-deploy

13. Start your containers

        $ mage-up

14. ???        


15. PROFIT.


### Host Requirements

- Git
- Docker
- Docker Compose
- NFS support


> **_./mage_aliases.sh_** enables the following aliases:

- `mage-bin`: runs the Magento utility from the CLI container
- `mage-cli-exec`: runs a command inside the PHP CLI container
- `mage-cli-root`: access PHP CLI container as root
- `mage-cli`: access PHP CLI container
- `mage-compile`: runs the compile command from the CLI container
- `mage-compose`: runs `docker-compose` from any folder
- `mage-cron`: starts the cron container
- `mage-db-exec`: runs MySQL commands inside the container
- `mage-db`: access mysql container
- `mage-deploy`: runs the deploy command from the CLI container
- `mage-down`: stops and removes all project containers (accepts -v parameter to also drop volumes)
- `mage-flush-dirs`: cleans up the Magento's auto-generated files 
- `mage-fpm`: access PHP FPM container
- `mage-go`: changes directory to the source code of the project
- `mage-http`: access http server container
- `mage-install`: runs the install command from the CLI container
- `mage-logs`: starts following docker logs from all containers (CTRL+C to stop)
- `mage-n98`: runs the n98 utility from the CLI container
- `mage-run`: runs the magetwo tools from anywhere
- `mage-start`: starts all containers
- `mage-stop`: stop all (you may provide a container name) containers
- `mage-up-logs`: starts all containers and follows their logs
- `mage-up`: create (if needed) and start all (you may provide a container name) containers (detached)
- `mage-upgrade`: runs the upgrade command from the CLI container
- `mage-which`: displays the current values of the environment variables set in the aliases file
#
#

> **PHP CLI Container**

- Command `flushdirs`: Clears some Magento _generated_, _var_ and _pub_ directories.
- Command `compile`: Clears _generated_ directory and compiles Magento.
- Command `deploy`: Clears some Magento _var_ and _pub_ directories, flushs the cache, upgrades the modules, compiles Magento, runs translation and deploys static content.
- Command `install`: runs Magento install command with preset parameters
- Command `mage` / `magento`: runs bin/magento
- Command `mod`: File mode in octal
- Command `n98`: Runs n98 utility
- Command `upgrade`: Clears some Magento _var_ directories, flushs the cache and upgrades the modules.
- Command `opened-ports`: Lists all TCP ports that are being monitorated by any application
#
#

> **MySQL Container**

- Command `db`: Log's in to MySQL selecting the _magento_ database
- Command `dbroot`: Log's in to MySQL selecting the _magento_ database with adminstrative privileges
- Command `dbdump`: Runs to MySQL Dump utility selecting the _magento_ database
