upstream fastcgi_backend {
    server php-fpm:9000;
}

## Multi shop code configuration
map $http_host $MAGE_RUN_CODE {
    hostnames;

    default base;
}

map $http_host $MAGE_RUN_TYPE {
    hostnames;

    default website;
}

## Map status code to exclude from access log
map $status $writelog { 429 0; 444 0; default 1; }

## Map HSTS header
map $scheme $hsts_header { https  "max-age=31556926; includeSubDomains; preload"; }

## Map bad user agents
map $http_user_agent $bad_client {
    default 0;
    ~*(360Spider)  1;
    ~*(Aboundex|Applebot|aiHitBot|AhrefsBot) 1;
    ~*(betaBot|BigBozz|BlackWidow|Bolt|BLEXBot|BUbiNG) 1;
    ~*(CazoodleBot|cispa|CPython|CCBot|ChinaClaw|Cliqzbot|coccocbot|Curious|CRAZYWEBCRAWLER|Custo) 1;
    ~*(Daumoa|Default|DeuSu|DIIbot|DISCo|discobot|DoCoMo|DuckDuckGo) 1;
    ~*(EasouSpider|eCatch|ecxi|EirGrabber|EmailCollector|EmailSiphon|EmailWolf|ExtractorPro|Exabot|Exploratodo|EyeNetIE) 1;
    ~*(FatBot|FlashGet|Findxbot) 1;
    ~*(GetRight|GetWeb!|GigablastOpenSource|Gigabot|gimme60bot|Go!Zilla|Go-Ahead-Got-It|Go.*package.*|GrabNet|Grafula|GT::WWW|GuzzleHttp) 1;
    ~*(heritrix|HaosouSpider|HMView|HTTP::Lite|HTTrack) 1;
    ~*(ia_archiver|IDBot|id-search|id-search.org|InterGET|InternetSeer.com|IRLbot) 1;
    ~*(JetCar) 1;
    ~*(larbin|LeechFTP|Lightspeedsystems|litemage_walker|Link|LinksManager.com_bot|Lipperhey|LinkpadBot|linkwalker|lwp-trivial|ltx71) 1;
    ~*(Maxthon$|Mail.RU_Bot|meanpathbot|MegaIndex.ru|MFC_Tear_Sample|microsoft.url|Microsoft-IIS|Microsoft.*Office|Mozilla.*Indy|Mozilla.*NEWT|MJ12bot|MSFrontPage) 1;
    ~*(Navroad|NearSite|NetAnts|NetLyzer.*FastProbe|NetSpider|NetZIP|Nutch) 1;
    ~*(Octopus) 1;
    ~*(PageGrabber|panscient.com|pavuk|PECL::HTTP|PeoplePal|pcBrowser|Pi-Monster|PHPCrawl|PleaseCrawl|psbot|prijsbest|python-requests) 1;
    ~*(Qwantify) 1;
    ~*(RealDownload|ReGet|RedesScrapy|Rippers|RocketCrawler) 1;
    ~*(SBIder|Scrapy|ScreenerBot|SEOprofiler|Screaming.*Spider|SeaMonkey|SeznamBot|SemrushBot|sitecheck.internetseer.com|SiteSnagger) 1;
    ~*(SmartDownload|Snoopy|SputnikBot|Steeler|SuperBot|SuperHTTP|Surfbot|sqlmap) 1;
    ~*(tAkeOut|Teleport|Toata|TwengaBot|Typhoeus) 1;
    ~*(URI::Fetch|User-Agent|UserAgent) 1;
    ~*(voltron|Vagabondo|VoidEYE|Visbot) 1;
    ~*(webalta|WebAuto|[Ww]eb[Bb]andit|WebCollage|WebCopier|WebFetch|WebLeacher|WebReaper|WebSauger|WebStripper|WebWhacker|WhatsApp) 1;
    ~*(WebZIP|Wget|Widow|Wotbox|WWW-Mechanize|WWWOFFLE) 1;
    ~*(XoviBot) 1;
    ~*(zermelo|Zeus|Zeus.*Webster|zgrab|ZyBorg) 1;
}

server {
    listen 80;
    listen 443 ssl;

    server_name _;

    ## Logs
#    access_log  /var/log/nginx/access.log  main  if=$writelog;
    access_log  /var/log/nginx/access.log  main;
    error_log   /var/log/nginx/error.log   error;

    if ($bad_client) { return 444; }

    ##################################
    ## Set Magento root folder
    set $MAGE_ROOT /var/www/html;

    ## Set main public directory /pub
    root $MAGE_ROOT/pub;

    ## Set Magento mode
    set $MAGE_MODE developer;
    ##################################

    # Support for SSL termination.
    set $my_http "http";
    set $my_ssl "off";
    set $my_port "80";
    if ($http_x_forwarded_proto = "https") {
        set $my_http "https";
        set $my_ssl "on";
        set $my_port "443";
    }

    ssl_certificate /etc/nginx/ssl/app.crt;
    ssl_certificate_key /etc/nginx/ssl/app.key;

    index index.php;
    autoindex off;
    charset UTF-8;
    client_max_body_size 64M;
    error_page 404 403 = /errors/404.php;
    #add_header "X-UA-Compatible" "IE=Edge";

    # Deny access to sensitive files
    location /.user.ini {
        deny all;
    }

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location /pub/ {
        location ~ ^/pub/media/(downloadable|customer|import|theme_customization/.*\.xml) {
            deny all;
        }
        alias $MAGE_ROOT/pub/;
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /static/ {
        if ($MAGE_MODE = "production") {
            expires max;
        }

        # Remove signature of the static files that is used to overcome the browser cache
        location ~ ^/static/version {
            rewrite ^/static/(version\d*/)?(.*)$ /static/$2 last;
        }

        location ~* \.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2|json)$ {
            add_header Cache-Control "public";
            add_header X-Frame-Options "SAMEORIGIN";
            expires +1y;

            if (!-f $request_filename) {
                rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
            }
        }
        location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
            add_header Cache-Control "no-store";
            add_header X-Frame-Options "SAMEORIGIN";
            expires    off;

            if (!-f $request_filename) {
                rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
            }
        }
        if (!-f $request_filename) {
            rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
        }
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /media/ {
        try_files $uri $uri/ /get.php$is_args$args;

        location ~ ^/media/theme_customization/.*\.xml {
            deny all;
        }

        location ~* \.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$ {
            add_header Cache-Control "public";
            add_header X-Frame-Options "SAMEORIGIN";
            expires +1y;
            try_files $uri $uri/ /get.php$is_args$args;
        }
        location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
            add_header Cache-Control "no-store";
            add_header X-Frame-Options "SAMEORIGIN";
            expires    off;
            try_files $uri $uri/ /get.php$is_args$args;
        }
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /media/customer/ {
        deny all;
    }

    location /media/downloadable/ {
        deny all;
    }

    location /media/import/ {
        deny all;
    }

    location /errors/ {
        location ~* \.xml$ {
            deny all;
        }
    }

    # PHP entry point for main application
    location ~ ^/(index|get|static|errors/report|errors/404|errors/503|health_check)\.php$ {
        try_files $uri =404;
        fastcgi_pass   fastcgi_backend;
        fastcgi_buffers 1024 4k;

        ## Store code with multi domain
        fastcgi_param  MAGE_RUN_CODE  $MAGE_RUN_CODE;
        fastcgi_param  MAGE_RUN_TYPE  $MAGE_RUN_TYPE;

        fastcgi_param  PHP_FLAG  "session.auto_start=off \n suhosin.session.cryptua=off";
        fastcgi_param  PHP_VALUE "memory_limit=768M \n max_execution_time=18000";
        fastcgi_read_timeout 4800s;
        fastcgi_connect_timeout 4800s;
        fastcgi_param  MAGE_MODE $MAGE_MODE;

        # Magento uses the HTTPS env var to detrimine if it is using SSL or not.
        fastcgi_param  HTTPS $my_ssl;

        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }

    gzip on;
    gzip_disable "msie6";

    gzip_comp_level 6;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_proxied any;
    gzip_types
        text/plain
        text/css
        text/js
        text/xml
        text/javascript
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/xml+rss
        image/svg+xml;
    gzip_vary on;

    # Banned locations (only reached if the earlier PHP entry point regexes don't match)
    location ~* (\.php$|\.htaccess$|\.git) {
        deny all;
    }
}
